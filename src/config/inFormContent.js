
export const EN_CONTENT = {
    "name": {
        "label": "Name",
        "errorMsg": "Please enter your name"
    },
    "email": {
        "label": "Email ID",
        "errorMsgEmpty": "Please enter your email address",
        "errorMsgInvalid": "Please enter a valid email address"
    },
    "phone": {
        "label": "Phone number",
        "errorMsgEmpty": "Please enter a mobile number",
        "errorMsgInvalid": "Please enter a valid mobile number"
    },
    "whatsapp": {
        "label": "Send me updates on WhatsApp"
    },
    "property": {
        "label": "Property Name",
        "propertyNotFoundText": "Could not find your property?",
        "enterAddressText": "Enter full address of the property",
        "errorMsg": "Please enter a property name"
    },
    "pincode": {
        "label": "Enter pincode",
        "errorMsgEmpty": "Please enter pincode",
        "errorMsgInvalid": "Please enter valid pincode"
    },
    "tnc": {
        "subText": "By submitting this form, you agree to the",
        "privacyLabel": "privacy policy",
        "tncLabel": "terms and conditions"
    }
}


export const AR_CONTENT = {
    "title": "استشارة مصمم",
    "name": {
        "label": "الاسم",
        "errorMsg": "من فضلك أدخل إسمك"
    },
    "email": {
        "label": "البريد الإلكتروني",
        "errorMsgEmpty": "يُرجى إدخال عنوان بريدك الإلكتروني",
        "errorMsgInvalid": "يُرجى إدخال عنوان بريد إلكتروني صحيح"
    },
    "phone": {
        "label": "رقم الهاتف",
        "errorMsgEmpty": "يُرجى إدخال رقم الهاتف الجوال",
        "errorMsgInvalid": "يُرجى إدخال رقم جوال صحيح"
    },
    "whatsapp": {
        "label": "الحصول على آخر المستجدات عبر تطبيق واتساب"
    },
    "property": {
        "label": "اسم العقار",
        "propertyNotFoundText": "لا يمكن العثور على العقار الخاص بك؟",
        "enterAddressText": "أدخل العنوان الكامل للعقار",
        "errorMsg": "الرجاء إدخال اسم الخاصية"
    },
    "pincode": {
        "label": "الرمز البريدي",
        "errorMsgEmpty": "يُرجى إدخال الرمز السري",
        "errorMsgInvalid": "يُرجى إدخال رمز سري صحيح"
    },
    "tnc": {
        "subText": "يُعد تقديم هذا النموذج موافقةً منك على",
        "privacyLabel": "سياسة الخصوصية",
        "tncLabel": "الشروط والأحكام"
    }
}
