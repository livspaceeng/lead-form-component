export default {
    props: {
        newProjectInfo: {
            type: Object,
            required: true
        },
        isoCode: {
            type: String,
            default: 'IN'
        },
        viewerCountryCode: {
            type: String,
            default: ''
        },
        properties: {
            type: Array,
            default: () => ([])
        },
        property_detail: {
            type: Object,
            default: () => ({})
        },
        pincode_list: {
            type: Array,
            default: () => ([])
        },
        submit_button_text: {
            type: String,
            default: 'GET FREE QUOTE'
        },
        form_title: {
            type: String,
            default: 'Talk to a designer'
        },
        readonly_fields: {
            type: Array,
            default: () => ([])
        },
        term_condition_url: {
            type: String,
            default: '/in/service'
        },
        privacy_policy_url: {
            type: String,
            default: '/in/privacy'
        },
        notServiceableMsg: {
            type: String,
            default: 'Sorry, we are currently not serviceable in this locality.'
        },
        enabledFlags: {
            type: Boolean,
            default: true
        }, 
        disableLeadFormCta: {
            type: Boolean,
            default: false
        },
        validatephone: {
            type: Function,
            required: true
        },
        submit: {
            type: Function,
            required: true
        },
        selectPlace: {
            type: Function,
            required: true
        },
        showPlaceList: {
            type: Function,
            required: true
        },
        ValidationError: {
            type: Function,
            required: true
        },
        handleFieldChange:{
            type: Function
        },
        //fieldId is used to identify fields uniquely when there are more than one form on same page
        fieldId: {
            type: String,
            default: ''
        },
        layoutDirection: {
            type: String,
            default: 'ltr'
        },
        disablePhone: {
            type:Boolean,
            default:false
        },
        disableEmail: {
            type:Boolean,
            default:false
        },
        disabledFormField: {
            type: Object,
            default: null
        },
        propertyLabel: {
            type: String,
            default: null
        }
    },
    mounted() {
        if(this.newProjectInfo && this.newProjectInfo.phone) {
            this.phoneValidationData['val'] = this.newProjectInfo.phone;
            this.phoneValidationData['obj'] = {
                'iso2': (this.viewerCountryCode || this.isoCode || 'in').toLowerCase()
            }
            this.validatePhoneFromApi()
        }
    },
    computed: {
        isRtlLayout() {
            return this.layoutDirection === 'rtl' || false
        },
        propertyFieldDisabled() {
            return this.disabledFormField
                && this.disabledFormField['propertyField']
        }
    },
    methods: {
        validatephoneAndUpdateError (val, obj) {
            this.phoneValidationData['val'] = val;
            this.phoneValidationData['obj'] = obj;
            this.newProjectInfo["phone"] = (val && val.includes('+')) ? val : "+" + obj.dialCode + val;
            this.$forceUpdate();
        },
        async validatePhoneFromApi () {
            let val = this.phoneValidationData['val'];
            let obj = this.phoneValidationData['obj'];
            //remove spaces from the phone number
            //Sample: +91 80958 52546 -> +918095852546
            val = (val || '').split(' ').join('')
            this.newProjectInfo["phone"] = val;
            let isValid = await this.validatephone(val, obj);
            this.isPhoneValid = isValid;
            if(isValid) {
                this.errors['phone'] = {}
                this.errors['phone']['invalid'] = false;
            } else {
                this.errors['phone'] = {}
                this.errors['phone']['invalid'] = true
            }

            if(obj && obj.iso2 == "in" && val && val[0] && val[0] === '0') {
                // remove 0 after +91 when google auto fills phone number if any
                // Sample: +9108095852546 -> +918095852546
                this.newProjectInfo["phone"] = `+${obj.dialCode}${val.slice(1, val.length)}`
            }
            this.handleFieldOnBlur('phoneNo',this.newProjectInfo["phone"]);
            this.$forceUpdate();
        },
        readonly (field) {
            if (this.readonly_fields.includes(field)) {
                 return true
            } else {
                 return false
            }
        },
        alphabetValid(val){
            let data  = val.trim();
            if( !data ){
                return false;
            }
            return true;
        },
        isValidPincode(val){
            let pinCodeRegex = /^[1-9][0-9]{5}$/;
            let fiveDigitPincodeCountries = ['my', 'sa']
            let countryCode = (this.viewerCountryCode && this.viewerCountryCode.toLowerCase()) || this.isoCode.toLowerCase();
            if(fiveDigitPincodeCountries.includes(countryCode))
                pinCodeRegex = /^[1-9][0-9]{4}$/
            return pinCodeRegex.test(val);
        },
        trimEmail () {
            this.newProjectInfo.email = this.newProjectInfo.email ? this.newProjectInfo.email.trim() : '';
        },
        handleEmailFieldOnBlur() {
            this.newProjectInfo.email = this.newProjectInfo.email && this.newProjectInfo.email.trim();
            this.handleFieldOnBlur('email',this.newProjectInfo.email);
        },
        handleFieldOnBlur(fieldName,fieldValue){
            this.handleFieldChange && this.handleFieldChange({fieldName, fieldValue})
        },                                       
    },
    watch: {
        properties: function () {
            this.showListOfPlaces()
        },
        property_detail: function () {
            this.populateSelectedPlace()
        },
        newProjectInfo: function(newVal) {
            if(!this.whatsappCheckedByUser) {
                this.whatsappChecked = newVal.whatsapp_alert || false
            }
            if(!this.consentCheckedByUser) {
                this.consentChecked = newVal.consent_checked || false
            }
        }
    }
}